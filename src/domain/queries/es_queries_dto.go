package queries

type EsQuery struct {
	Equals []FieldValue `json:"equals"`
}

type EsQuery2 struct {
	Size int `json:"size"`
	//Equals []FieldValue2 `json:"equals"`
}

type FieldValue struct {
	Field string `json:"field"`
	Value interface{} `json:"value"`
}

//type FieldValue2 struct {
//	Field string `json:"field"`
//	Value interface{} `json:"value"`
//}
