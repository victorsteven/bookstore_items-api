package queries

import "github.com/olivere/elastic/v7"

func (q EsQuery) Build() elastic.Query {
	query :=  elastic.NewBoolQuery()
	if len(q.Equals) > 0 {
	}
	equalQueries := make([]elastic.Query, 0)
	for _, es := range q.Equals {
		equalQueries = append(equalQueries, elastic.NewMatchQuery(es.Field, es.Value))
	}
	query.Must(equalQueries...)
	return query
}

func (q EsQuery2) Build2() elastic.Query {
	query :=  elastic.NewBoolQuery()

	//equalQueries := make([]elastic.Query, 0)
	//for _, es := range q.Equals {
	//	equalQueries = append(equalQueries, elastic.NewMatchQuery(es.Field, es.Value))
	//}
	//query.Must(equalQueries...)
	return query
}
