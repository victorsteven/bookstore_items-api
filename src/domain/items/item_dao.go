package items

import (
	"bookstore_items-api/src/clients/elasticsearch"
	"bookstore_items-api/src/domain/queries"
	"bookstore_utils-go/rest_errors"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
)

const (
	indexItems = "items"
)

func (i *Item) Save() rest_errors.RestErr {
	result, err := elasticsearch.Client.Index(indexItems, i)
	if err != nil {
		return rest_errors.NewInternalServerError("error when trying to save item", errors.New("database error"))
	}
	i.Id = result.Id //update the id with the one from elastic search
	return nil
}
func (i *Item) Get() rest_errors.RestErr {
	itemId := i.Id
	result, err := elasticsearch.Client.Get(indexItems, i.Id)
	if err != nil {
		if strings.Contains(err.Error(), "404") {
			return rest_errors.NewNotFoundError(fmt.Sprintf("no item found with id %s", i.Id))
		}
		return rest_errors.NewInternalServerError( fmt.Sprintf("error when trying to get id %s", i.Id), errors.New("database error"))
	}
	fmt.Println(result.Error)
	bytes, err := result.Source.MarshalJSON()
	if err != nil {
		return rest_errors.NewInternalServerError("error when trying to parse database response", errors.New("database error"))
	}
	if err := json.Unmarshal(bytes, &i); err != nil {
		return rest_errors.NewInternalServerError("error when trying to parse database response", errors.New("database error"))
	}
	i.Id = itemId

	fmt.Println(err)
	fmt.Println(string(bytes))
	return nil
}

//func (i *Item) GetAll() rest_errors.RestErr {
//	//itemId := i.Id
//	result, err := elasticsearch.Client.GetAll(indexItems)
//	if err != nil {
//		if strings.Contains(err.Error(), "404") {
//			return rest_errors.NewNotFoundError(fmt.Sprintf("no item found with id %s", i.Id))
//		}
//		return rest_errors.NewInternalServerError( fmt.Sprintf("error when trying to get id %s", i.Id), errors.New("database error"))
//	}
//	fmt.Println(result.Error)
//	bytes, err := result.Source.MarshalJSON()
//	if err != nil {
//		return rest_errors.NewInternalServerError("error when trying to parse database response", errors.New("database error"))
//	}
//	if err := json.Unmarshal(bytes, &i); err != nil {
//		return rest_errors.NewInternalServerError("error when trying to parse database response", errors.New("database error"))
//	}
//	//i.Id = itemId
//
//	fmt.Println(err)
//	fmt.Println(string(bytes))
//	return nil
//}

func (i *Item) Search(query queries.EsQuery) ([]Item, rest_errors.RestErr) {
	result, err := elasticsearch.Client.Search(indexItems, query.Build())
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error when trying to search documents", errors.New("database error"))
	}
	fmt.Println(result)

	items := make([]Item, result.TotalHits())
	for index, hit := range result.Hits.Hits {
		bytes, _ := hit.Source.MarshalJSON()
		var item Item
		if err := json.Unmarshal(bytes, &item); err != nil {
			return nil, rest_errors.NewInternalServerError("error when trying to parse response", errors.New("database error"))
		}
		item.Id = hit.Id
		items[index] = item
	}
	if len(items) == 0 {
		return nil, rest_errors.NewNotFoundError("no item found matching given criteria")
	}
	return items, nil
}

//func (i *Item) SearchAll(query queries.EsQuery2) ([]Item, rest_errors.RestErr) {
//	result, err := elasticsearch.Client.SearchAll(indexItems, query.Build2())
//	if err != nil {
//		return nil, rest_errors.NewInternalServerError("error when trying to search documents", errors.New("database error"))
//	}
//	items := make([]Item, result.TotalHits())
//	for index, hit := range result.Hits.Hits {
//		bytes, _ := hit.Source.MarshalJSON()
//		var item Item
//		if err := json.Unmarshal(bytes, &item); err != nil {
//			return nil, rest_errors.NewInternalServerError("error when trying to parse response", errors.New("database error"))
//		}
//		item.Id = hit.Id
//		items[index] = item
//	}
//	fmt.Println("The hits: ", items)
//	if len(items) == 0 {
//		return nil, rest_errors.NewNotFoundError("no item found matching given criteria")
//	}
//	return items, nil
//}

func (i *Item) SearchAll() ([]Item, rest_errors.RestErr) {
	result, err := elasticsearch.Client.SearchAll(indexItems)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error when trying to search documents", errors.New("database error"))
	}
	items := make([]Item, result.TotalHits())
	for index, hit := range result.Hits.Hits {
		bytes, _ := hit.Source.MarshalJSON()
		var item Item
		if err := json.Unmarshal(bytes, &item); err != nil {
			return nil, rest_errors.NewInternalServerError("error when trying to parse response", errors.New("database error"))
		}
		item.Id = hit.Id
		items[index] = item
	}
	fmt.Println("The hits: ", items)
	if len(items) == 0 {
		return nil, rest_errors.NewNotFoundError("no item found matching given criteria")
	}
	return items, nil
}