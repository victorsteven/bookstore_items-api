package elasticsearch

import (
	"bookstore_utils-go/logger"
	"context"
	"fmt"
	"github.com/olivere/elastic/v7"
	"time"
)

var (
	Client esClientInterface = &esClient{}
)

// Get tweet with specified ID
//ctx := context.Background()
//get1, err := client.Get().
//Index("twitter").
//Type("tweet").
//Id("1").
//Do(ctx)
//if err != nil {
//// Handle error
//panic(err)
//}
//if get1.Found {
//fmt.Printf("Got document %s in version %d from index %s, type %s\n", get1.Id, get1.Version, get1.Index, get1.Type)
//}

type esClientInterface interface {
	setClient(*elastic.Client)
	Index(string, interface{}) (*elastic.IndexResponse, error)
	Get(string, string) (*elastic.GetResult, error)
	//GetAll(string) (*elastic.GetResult, error)
	Search(string, elastic.Query) (*elastic.SearchResult, error)
	//SearchAll(string, elastic.Query) (*elastic.SearchResult, error)
	SearchAll(string) (*elastic.SearchResult, error)

}

type esClient struct {
	client *elastic.Client
}

func (c *esClient) setClient(client *elastic.Client) {
	c.client = client
}

func Init() {
	log := logger.GetLogger()
	client, err := elastic.NewClient(
		 elastic.SetURL("http://127.0.0.1:9200"),
		 elastic.SetSniff(false),
		 elastic.SetHealthcheckInterval(10*time.Second),
		 elastic.SetErrorLog(log),
		 elastic.SetInfoLog(log),
		 //elastic.SetHeaders(http.Header{
			// "X-Caller-Id": []string{"..."},
		 //}),
	 )
	 if err != nil {
	 	panic(err)
	 }
	 Client.setClient(client)
 }

 func (c *esClient) Index(index string, doc interface{})  (*elastic.IndexResponse, error) {
 	ctx := context.Background()
 	result, err :=  c.client.Index().Index(index).BodyJson(doc).Do(ctx)
 	if err != nil {
 		logger.Error(fmt.Sprintf("error when trying to index document in index %s ", index), err)
 		return nil, err
	}
	return result, nil
 }

 func (c *esClient) Get(index string, id string) (*elastic.GetResult, error) {
	ctx := context.Background()
	result, err := c.client.Get().Index(index).Id(id).Do(ctx)
	if err != nil {
		//fmt.Println(result.Found)
		logger.Error(fmt.Sprintf("error when trying to get id %s", id), err)
		return nil, err
	}
	//if !result.Found {
	//	return nil, nil
	//}
	return result, nil
 }

 func (c *esClient) Search(index string, query elastic.Query) (*elastic.SearchResult, error) {
	ctx := context.Background()
	//if err := c.client.Search(index).Query(query).Validate(); err != nil {
	//	fmt.Println("result: ", err.Error())
	//	return nil, nil
	//}
	result, err := c.client.Search(index).Query(query).Do(ctx)
	if err != nil {
		logger.Error(fmt.Sprintf("error when trying to search documents in index %s", index), err)
		return nil, err
	}

	return result, nil
 }


func (c *esClient) SearchAll(index string) (*elastic.SearchResult, error) {
	ctx := context.Background()

	result, err := c.client.Search(index).Do(ctx)

	if err != nil {
		logger.Error(fmt.Sprintf("error when trying to search documents in index %s", index), err)
		return nil, err
	}
	fmt.Println("the call went through")

	return result, nil
}

//func (c *esClient) SearchAll(index string, query elastic.Query) (*elastic.SearchResult, error) {
//	ctx := context.Background()
//
//	termQuery := elastic.NewTermQuery("size", 12)
//
//	result, err := c.client.Search(index).Query(termQuery).Do(ctx)
//	//result, err := c.client.Search("items").Size(1).Do(ctx)
//	//fmt.Println("the result: ", result.Hits.Hits)
//
//	if err != nil {
//		logger.Error(fmt.Sprintf("error when trying to search documents in index %s", index), err)
//		return nil, err
//	}
//	fmt.Println("the call went through")
//
//	return result, nil
//}