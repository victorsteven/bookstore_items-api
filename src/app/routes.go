package app

import (
	"bookstore_items-api/src/controllers"
	"net/http"
)

func mapUrls(){
	router.HandleFunc("/items", controllers.ItemsController.Create).Methods(http.MethodPost)
	router.HandleFunc("/items/{id}", controllers.ItemsController.Get).Methods(http.MethodGet)
	router.HandleFunc("/items/search", controllers.ItemsController.Search).Methods(http.MethodPost)
	//router.HandleFunc("/items/search_all", controllers.ItemsController.SearchAll).Methods(http.MethodPost)
	//router.HandleFunc("/items/search_all", controllers.ItemsController.SearchAll).Methods(http.MethodPost)
	router.HandleFunc("/items_all/search_all", controllers.ItemsController.SearchAll).Methods(http.MethodGet) //the url is important to avoid matching the wild card above



	//router.HandleFunc("/items", controllers.ItemsController.GetAll).Methods(http.MethodGet)
}