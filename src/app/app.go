package app

import (
	elasticsearch2 "bookstore_items-api/src/clients/elasticsearch"
	"github.com/gorilla/mux"
	"net/http"
)

var (
	router = mux.NewRouter()
)
func StartApp() {
	elasticsearch2.Init()
	mapUrls()

	srv := &http.Server{
		Addr:              "127.0.0.1:8080",
		Handler:           router,
	}
	if err := (srv.ListenAndServe()); err != nil {
		panic(err)
	}
}