package services

import (
	"bookstore_items-api/src/domain/items"
	"bookstore_items-api/src/domain/queries"
	"bookstore_utils-go/rest_errors"
	"fmt"
)
var (
	ItemsService itemsServiceInterface = &itemService{} //make sure u assign this, if not, the interface methods will return nil
)
type itemsServiceInterface interface {
	Create(items.Item) (*items.Item, rest_errors.RestErr)
	Get(string) (*items.Item, rest_errors.RestErr)
	//GetAll(string) ([]*items.Item, rest_errors.RestErr)
	Search(queries.EsQuery) ([]items.Item, rest_errors.RestErr)
	//SearchAll(queries.EsQuery2) ([]items.Item, rest_errors.RestErr)
	SearchAll() ([]items.Item, rest_errors.RestErr)
}

type itemService struct{}

func (s *itemService) Create(item items.Item) (*items.Item, rest_errors.RestErr) {
	if err := item.Save(); err != nil {
		return nil, err
	}
	return &item, nil
}

//func (s *itemService) GetAll(string) (*items.Item, rest_errors.RestErr) {
//	//allItems := []items.Item{}
//	item := items.Item{}
//
//	if err := item.GetAll(); err != nil {
//		fmt.Println("this is the bitch: ", err)
//		return nil, err
//	}
//	return &item, nil
//}

func (s *itemService) Get(id string) (*items.Item, rest_errors.RestErr) {
	item := items.Item{Id: id}
	if err := item.Get(); err != nil {
		fmt.Println("this is the bitch: ", err)
		return nil, err
	}
	return &item, nil
	//return nil, rest_errors.NewRestError("implement me", http.StatusNotImplemented, "not_implemented", nil  )
}

func (s *itemService) Search(query queries.EsQuery) ([]items.Item, rest_errors.RestErr) {
	dao := items.Item{}
	return dao.Search(query)
}

//func (s *itemService) SearchAll(query queries.EsQuery2) ([]items.Item, rest_errors.RestErr) {
//	dao := items.Item{}
//	return dao.SearchAll(query)
//}

func (s *itemService) SearchAll() ([]items.Item, rest_errors.RestErr) {
	dao := items.Item{}
	return dao.SearchAll()
}