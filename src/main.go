package main

import (
	"bookstore_items-api/src/app"
	"fmt"
	"github.com/gorilla/mux"
)

var (
	router = mux.NewRouter()
)
func main(){
	fmt.Println("the app is up")
	app.StartApp()
}
