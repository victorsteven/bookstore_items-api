package controllers

import (
	"bookstore_items-api/src/domain/items"
	"bookstore_items-api/src/domain/queries"
	"bookstore_items-api/src/services"
	"bookstore_items-api/src/utils/http_utils"
	"bookstore_oauth-go/oauth"
	"bookstore_utils-go/rest_errors"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strings"
	//"bytes"
)

//var buf bytes.Buffer


var (
	ItemsController itemsControllerInterface = &itemsController{}
)
type itemsControllerInterface interface {
	Create(http.ResponseWriter, *http.Request)
	Get(http.ResponseWriter, *http.Request)
	Search(http.ResponseWriter, *http.Request)
	SearchAll(http.ResponseWriter, *http.Request)


}
type itemsController struct{}

func (c *itemsController) Create(w http.ResponseWriter, r *http.Request) {
	fmt.Println("THE ITEM'S CREATE")
	if err := oauth.AuthenticateRequest(r); err != nil {
		fmt.Println("this is the error control: ", err)
		//w.Header().Set("Content-Type", "application/json")
		//w.WriteHeader(err.Status())
		//json.NewEncoder(w).Encode(err.Message())
		return
	}
	//sellerId := oauth.GetCallerId(r)
	//if sellerId == 0 {
	//	respErr := rest_errors.NewUnauthorizedError("enable to retrieve access token")
	//	http_utils.RespondError(w, respErr)
	//	return
	//}
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		respErr := rest_errors.NewBadRequestError("invalid request body")
		http_utils.RespondError(w, respErr)
		return
	}
	defer r.Body.Close()

	var itemRequest items.Item
	if err := json.Unmarshal(requestBody, &itemRequest); err != nil {
		respErr := rest_errors.NewBadRequestError("invalid json body")
		http_utils.RespondError(w, respErr)
		return
	}
	itemRequest.Seller = 1

	result, createErr := services.ItemsService.Create(itemRequest)
	if createErr != nil {
		http_utils.RespondError(w, createErr)
		return
	}
	http_utils.RespondJson(w, http.StatusCreated, result)
}

func (c *itemsController) Get(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	itemId := strings.TrimSpace(vars["id"])
	item, err := services.ItemsService.Get(itemId)
	if err != nil {
		http_utils.RespondError(w, err)
		return
	}
	http_utils.RespondJson(w, http.StatusOK, item)
}

//func (c *itemsController) GetAll(w http.ResponseWriter, r *http.Request) {
//	item, err := services.ItemsService.GetAll()
//	if err != nil {
//		http_utils.RespondError(w, err)
//		return
//	}
//	http_utils.RespondJson(w, http.StatusOK, item)
//}


func (c *itemsController) Search(w http.ResponseWriter, r *http.Request) {
	bytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		apiErr := rest_errors.NewBadRequestError("invalid json body")
		http_utils.RespondError(w, apiErr)
	}
	defer r.Body.Close()

	var query queries.EsQuery
	if err := json.Unmarshal(bytes, &query); err != nil {
		apiErr := rest_errors.NewBadRequestError("invalidd json body")
		http_utils.RespondError(w, apiErr)
		return
	}
	fmt.Println("the equal query passed: ", query)

	items, searchErr := services.ItemsService.Search(query)
	if searchErr != nil {
		http_utils.RespondError(w, searchErr)
		return
	}
	http_utils.RespondJson(w, http.StatusOK, items)
}

func (c *itemsController) SearchAll(w http.ResponseWriter, r *http.Request) {
	//bytes, err := ioutil.ReadAll(r.Body)
	//if err != nil {
	//	apiErr := rest_errors.NewBadRequestError("invalid json body")
	//	http_utils.RespondError(w, apiErr)
	//}
	//defer r.Body.Close()

	//fmt.Println("the byte string: ", string(bytes))

	//var query queries.EsQuery2
	//if err := json.Unmarshal(bytes, &query); err != nil {
	//	apiErr := rest_errors.NewBadRequestError("invalid json body")
	//	http_utils.RespondError(w, apiErr)
	//	return
	//}
	//fmt.Println("Query passed: ", query)
	items, searchErr := services.ItemsService.SearchAll()
	if searchErr != nil {
		http_utils.RespondError(w, searchErr)
		return
	}
	http_utils.RespondJson(w, http.StatusOK, items)
}