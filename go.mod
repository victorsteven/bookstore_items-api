module bookstore_items-api

go 1.13

require (
	bookstore_oauth-go v0.0.0
	bookstore_utils-go v0.0.0
	github.com/gorilla/mux v1.7.3
	github.com/olivere/elastic/v7 v7.0.9
	go.uber.org/zap v1.13.0
)

replace bookstore_oauth-go v0.0.0 => ../bookstore_oauth-go

replace bookstore_utils-go v0.0.0 => ../bookstore_utils-go
