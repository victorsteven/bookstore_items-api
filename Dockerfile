FROM golang:latest

#Install git
RUN apk update && apk add --no-cache git

#Where our file will be in the docker container
WORKDIR /usr/src/app

COPY go.mod go.sum ./

RUN go mod download

#Copy the source from the src directory to the working directory inside the container
COPY ./src .

RUN go build -o items-api .

EXPOSE 8080

CMD ["./items-api"]